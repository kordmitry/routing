<?php

use Framework\Routing\Router;
use Framework\Routing\Route;

Router::addRoute(new Route('hello', 'HelloController@index', Route::METHOD_GET));
Router::addRoute(new Route('user/{user_name}/group/{group_name}', 'HelloController@data', Route::METHOD_GET));